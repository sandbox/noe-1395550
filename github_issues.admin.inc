<?php

/**
 * @file
 * Administration page callbacks for the GitHub issues module.
 */

/**
 * Implements hook_admin_settings().
 */
function github_issues_admin_settings_form($form_state) {
  $form['github_issues_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('GitHub information'),
    '#description' => t('Fill in the form below. You will need a GitHub account'),
  );

  $form['github_issues_setting']['github_username'] = array(
    '#title'         => t('Username'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('github_issues_username'),
    '#size'          => 50,
    '#maxlength'     => 50,
    '#required'      => TRUE,
  );

  $form['github_issues_setting']['github_password'] = array(
    '#title'         => t('Password'),
    '#type'          => 'password',
    '#default_value' => variable_get('github_issues_password'),
    '#size'          => 50,
    '#maxlength'     => 50,
    '#required'      => TRUE,
  );

  $form['github_issues_setting']['github_organization'] = array(
    '#title'         => t('Organization'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('github_issues_organization'),
    '#size'          => 50,
    '#maxlength'     => 50,
    '#required'      => TRUE,
  );

  $form['#submit'][] = 'github_issues_admin_settings_form_submit';

  return system_settings_form($form);
}

function github_issues_admin_settings_form_submit($form, &$form_state) {
  watchdog('github_issues', print_r($form, TRUE));
  variable_set('github_issues_username', $form_state['values']['github_username']);
  variable_set('github_issues_password', $form_state['values']['github_password']);
  variable_set('github_issues_organization', $form_state['values']['github_organization']);
}
